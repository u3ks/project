import csv
from utilities.database import MongoPersister

def structure_european_season_name(date):

    date = date.split('/')
    month = int(date[1])
    year = int(date[2])
    seas_name = None
    if month >= 7 and month <= 12:
        seas_name =  str(year) + '/' + str(year + 1)
    elif month >= 1 and month <= 5:
        seas_name =  str(year-1)+ '/' + str(year)
    return seas_name


def getSeasonGID(persister, comp_code, seas_date):

    comp_id = persister.db['competitions'].find_one({"fdata_code":comp_code})['gid']
    seas_name = structure_european_season_name(seas_date)
    return comp_id, seas_name, int(str(comp_id) + seas_name.split('/')[0])


def generate_ids(persister, comp_file):

    reader = csv.DictReader(open(comp_file,'rb'))
    teams = set()
    refs = set()
    for index, row in enumerate(reader):

        if index == 0:
            comp_code = row['Div']
            seas_date = row['Date']
            comp_id, seas_name, seas_id = getSeasonGID(persister, comp_code, seas_date)

        if 'AwayTeam' in row.keys() and row['AwayTeam']: teams.add(row['AwayTeam'])
        if 'HomeTeam' in row.keys() and row['HomeTeam']: teams.add(row['HomeTeam'])
        if 'Referee' in row.keys() and  row['Referee'] : refs.add(row['Referee'].encode('utf8'))

    teams_stats_dic = {}
    for team in teams: teams_stats_dic[team] = {}
    teams = teams_stats_dic

    ref_stats_dic = {}
    for ref in refs: ref_stats_dic[ref] = {}
    refs = ref_stats_dic

    teams_dict, refs_dict = get_dict_with_ids(persister, len(refs))
    return comp_id, seas_name, seas_id, teams_dict, refs_dict, teams, refs


def generate_id(persister, id_type, data_dict, name):

    last_id = persister.db['id_generator'].find_and_modify({},{"$inc":{id_type:1}})
    data_dict[name] = last_id[id_type]


def get_dict_with_ids(persister, len_refs):

    refs_dict = {}
    teams_dict = {}

    if len_refs > 0:
        referees = persister.db['refs'].find({}, {"name":1, "gid":1})
        for ref in referees:
            refs_dict[ref['name']['fdata_name']] = ref['gid']


    teams_rs = persister.db['teams'].find({}, {"name":1, "gid":1})
    for team in teams_rs: teams_dict[team['name']['fdata_name']] = team['gid']

    return teams_dict, refs_dict


def getRoundGID(seas_id, index):

    index += 1
    round_str = str(index) if index > 9 else '0'+str(index)
    round_id = int(str(seas_id)+round_str)
    round_name = str(index) + 'th Round'
    if index == 1:
        round_name = '1st Round'
    elif index == 2:
        round_name = '2nd Round'
    elif index == 3:
        round_name = '3rd Round'
    return round_id, round_name


if __name__ == '__main__':
    persister = MongoPersister('db.prediction_model')
    persister.connect()
    generate_id(persister, 'referees', {}, "M. Atkinson")
    persister.close()