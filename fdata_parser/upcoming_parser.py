import csv
from mappings import odds_mapping
from utilities.parser import convert_data
from datetime import datetime, timedelta
from fdata_analyzer.predictor import EloPredictor
wanted_comps =['E0']


def parse_upcoming_fixtures(persister, comp_file, initial):

    '''

    Based on the odds mapping extract and structure the odds statistics
    from the file, add a prediction and save to database.

    @param persister: database connection
    @param comp_file: the football-data .csv file with odds data
    '''

    reader = csv.DictReader(open(comp_file,'rb'))
    predictor = EloPredictor(persister)
    for row in reader:
        parse_odds_and_add_to_db(persister, predictor, row, initial)


def parse_odds_and_add_to_db(persister, predictor, stats_dict, initial):


    r1 = None
    r2 = None

    if stats_dict['Div'] not in wanted_comps:
        return

    odds = {}
    for key in odds_mapping.keys():
        try:
            value = convert_data(stats_dict[key])
            odds[odds_mapping[key]] = value
        except KeyError:
            continue

    #skip games for which we already have the odds
    if not initial and odds['match_date'] < datetime.now():
        return

    if initial:
        rs = persister.db['fixtures'].find_one({"home_team.name.fdata_name":odds['home_team.name.fdata_name'],
                                                "away_team.name.fdata_name":odds['away_team.name.fdata_name'],
                                                "match_date": odds['match_date']},

                                               {"home_team.rating.pregame":1,
                                                "away_team.rating.pregame":1})

        r1 = rs['home_team']['rating']['pregame']
        r2 = rs['away_team']['rating']['pregame']

    try: predictor.predict(False, odds, r1, r2)
    except: pass

    persister.db['odds'].update({"home_team.name.fdata_name":odds["home_team.name.fdata_name"],
                                 "away_team.name.fdata_name":odds["away_team.name.fdata_name"],
                                 "match_date":odds['match_date']},
                                {"$set":odds}, upsert=True)