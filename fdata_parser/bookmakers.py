"""

    Dictionary that holds the basic information about the book makers whose
odds are going to be parsed and when ran as a script adds that information
to the database.

"""

from utilities.database import MongoPersister

bookies  = [

            {

             "name": "Bet365",
             "website": "http://bet365.com",
             "fdata_code":"B365",
             "gid": 1

             },

            {

             "name": "Blue Square",
             "fdata_code":"BS",
             "website": "https://www.bluesq.com/",
             "gid": 2

             },

            {

             "name": "Bet&Win",
             "fdata_code":"BW",
             "gid": 3

             },

            {

             "name": "Gamebookers",
             "fdata_code":"GB",
             "website": "https://sports.gamebookers.com/",
             "gid": 4

             },

            {

             "name": "Interwetten",
             "fdata_code":"IW",
             "website": "https://www.interwetten.com/",
             "gid": 5
             },


            {

             "name": "Ladbrokes",
             "fdata_code":"LB",
             "website": "http://ladbrokes.com/",
             "gid": 6

             },

            {

             "name": "Pinnacle Sports",
             "fdata_code":"PS",
             "website": "http://pinnaclesports.com/",
             "gid": 7

             },

            {

             "name": "Sporting Odds",
             "fdata_code":"SO",
             "gid": 8

             },

            {

             "name": "Sportingbet",
             "fdata_code":"SB",
             "website": "http://sportingbet.com",
             "gid": 10

             },

            {

             "name": "Stan James",
             "fdata_code":"SJ",
             "website": "http://stanjames.com",
             "gid": 11

             },

            {

             "name": "Stanleybet",
             "fdata_code":"SY",
             "website": "http://stanleybet.ro",
             "gid": 12

             },

            {

             "name": "Bet Victor",
             "fdata_code":"VC",
             "website": "http://betvictor.com",
             "gid": 13

             },

            {

             "name": "William Hill",
             "fdata_code":"WH",
             "website": "http://williamhill.com",
             "gid": 14

             }]


if __name__ == '__main__':

    persister = MongoPersister('db.prediction_model')
    persister.connect()
    for bookie in bookies:
        persister.db['bookmakers'].update({"gid":bookie['gid']},
                                            bookie,
                                            upsert=True)
    persister.close()
