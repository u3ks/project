from urllib import urlretrieve
from utilities.database import MongoPersister
from upcoming_parser import parse_upcoming_fixtures
from results_parser import parse_results_and_stats
from fdata_analyzer.rating_calculator import calculate_elo_points
from fdata_analyzer.available_seasons import available_seasons
from fdata_analyzer.predictor import EloPredictor
import glob

base_url = 'http://www.football-data.co.uk/mmz4281/'
url_end = '/E0.csv'
available_years = [  '0001','0102','0203','0304','0405','0506','0607',
                     '0708','0809','0910','1011','1112','1213','1314',
                     '1415']

base_save_path = 'D:/Betting/prem data/'

def parse_fdata(initial_setup=False):

    '''
    Create a database connection, download the data files, and pass each file to
    the appropriate parser.

    '''

    persister = MongoPersister('db.prediction_model')
    persister.connect()
    directories_to_be_parsed = download_files(initial_setup)
    for direct in directories_to_be_parsed:
        data_files = glob.glob(direct+'*')
        for comp_file in data_files:
            print 'Parsing ', comp_file
            parse_results_and_stats(persister, comp_file)
    calculate_elo_points(persister, initial_setup)

    persister.close()


def download_files(initial_setup):
    """

    Download files from the football-data servers and save them to
    base_save_path, then return the location of all the
    newly downloaded files.

    @param initial_setup: flag, whether to download and parse all data or
    just the most recent one.
    """

    seasons = available_years
    if not initial_setup:
        seasons = [available_years[len(available_years) - 1]]
    data_paths = []

    for year in seasons:
        file_url =  base_url + year + url_end
        urlretrieve(file_url, base_save_path + '/' + year +'.csv')
        data_paths.append(base_save_path + year + '.csv')

    return data_paths


def parse_fdata_odds(initial_setup=False):
    ''''

    Parse just the odds of the latest fixtures,
    if initial setup is true parse all files.

    @param initial_setup: flag, which files to download and parse.

    '''

    persister = MongoPersister('db.prediction_model')
    persister.connect()

    if initial_setup:
        directories_to_be_parsed = download_files(initial_setup)
        for direct in directories_to_be_parsed:
            data_files = glob.glob(direct+'*')
            for comp_file in data_files:
                parse_upcoming_fixtures(persister, comp_file, initial_setup)

    else:
        urlretrieve('http://www.football-data.co.uk/fixtures.csv', base_save_path + 'latest.csv')
        parse_upcoming_fixtures(persister,
                                base_save_path + 'latest.csv',
                                False)

    persister.close()


if __name__ == '__main__':

    parse_fdata()
    parse_fdata_odds()
