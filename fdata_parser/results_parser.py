import csv
from id_parser import generate_ids, getRoundGID, generate_id
from mappings import fixture_maping
from utilities.parser import convert_data
from mappings import initialize_record_field
from upcoming_parser import parse_odds_and_add_to_db
from fdata_analyzer.predictor import EloPredictor

def parse_results_and_stats(persister, comp_file, initial=False):

    '''

    For each fixture in the file, generate_ids for the season, competition,
    refs, teams and rounds.
    Extract the statistics for the fixture and add them to the
    season, round, team, ref.


    '''

    comp_id, seas_name, seas_id, teams_dict, refs_dict, teams, refs = generate_ids(persister, comp_file)
    fixtures_in_round = len(teams)/2
    reader = csv.DictReader(open(comp_file,'rb'))
    round_index = 0
    fgid = 0
    season = {'fixtures':[]}
    current_round = {'fixtures':[]}
    predictor = EloPredictor(persister)
    for index, row in enumerate(reader):
        fgid += 1

        skip = False

        for each in ['HomeTeam', 'AwayTeam', 'FTHG', 'FTAG']:
            if  each not in row.keys() or (not row[each]):
                skip = True

        if skip: continue

        fixture = {}
        for key in fixture_maping.keys():
            try:
                value = convert_data(row[key])
                fixture[fixture_maping[key]] = value
            except KeyError:
                continue

        ids_and_names = get_ids_and_names(persister, round_index, teams_dict, refs_dict, row,
                                          comp_id, seas_name, seas_id, fgid)
        fixture.update(ids_and_names)

        update_round_season_team_ref(fixture, teams, refs, current_round, season)
        season['fixtures'].append({"gid":fixture['gid']})
        current_round['fixtures'].append({"gid":fixture['gid']})

        persister.db['fixtures'].update({'gid':fixture['gid']},
                                        {"$set":fixture}, upsert=True)

        if initial:
            parse_odds_and_add_to_db(persister, predictor, row)

        if ((index+1) % fixtures_in_round == 0):
            persister.db['rounds'].update({'gid': current_round['gid']},
                                              {"$set": current_round},
                                                upsert=True)

            current_round = {'fixtures':[]}
            round_index += 1
            fgid = 1

    if 'gid' in season.keys():
        persister.db['seasons'].update({'gid':season['gid']},
                                        {"$set":season}, upsert=True)

    add_refs_and_teams_to_database(persister, teams, refs)


def update_round_season_team_ref(fixture,teams,refs,current_round,season):

    update_team('home', fixture, teams)
    update_team('away', fixture, teams)
    if 'referee.name.fdata_name' in fixture.keys() and fixture['referee.name.fdata_name']:
        update_ref(refs[fixture['referee.name.fdata_name']], fixture)
    update_part(current_round, fixture, True)
    update_part(season, fixture, False)


def update_team(team_key, fixture, teams):

    name = '%s_team.name.fdata_name'%team_key
    team = teams[fixture[name]]
    if 'record.$.season.gid' not in team.keys():
        team.update(initialize_record_field(fixture['season.gid'],
                                            fixture['competition.gid'],
                                            fixture['season.name.fdata_name'],
                                            True))
        team['name.fdata_name'] = fixture[name]
        team['gid'] = fixture[team_key+'_team.gid']

    update_record(team_key, fixture, team, True)


def update_record(name_key, fixture, data, team):

    opp_key = 'home'
    if name_key == 'home': opp_key = 'away'

    #available statistics
    keys = ['corners', 'fouls', 'cards', 'goals', 'shots']
    for key in keys:
        #the namespace of the statistics with regard to the fixture structure
        fixture_key = name_key + '_team.' + key
        #subfields for each statistic
        sub_fields = ['half_time', 'full_time']
        if key == 'cards': sub_fields = ['yellow', 'red']
        if key == 'shots': sub_fields = ['on_target', 'total']

        if key in ['cards', 'goals', 'shots']:
            for sub_field in sub_fields:
                fixture_field_key = fixture_key + '.' + sub_field
                opp_field_key = (opp_key + '_team.' + key + '.' + sub_field)
                if fixture_field_key not in fixture.keys() : continue
                data['record.$.'+name_key+'.'+key+'.'+sub_field+'.'+'for'] += fixture[fixture_field_key]
                data['record.$.'+name_key+'.'+key+'.'+sub_field+'.'+'against'] += fixture[opp_field_key]
                data['record.$.'+'total'+'.'+key+'.'+sub_field+'.'+'for'] += fixture[fixture_field_key]
                data['record.$.'+'total'+'.'+key+'.'+sub_field+'.'+'against'] += fixture[opp_field_key]

        else:
            if fixture_key not in fixture.keys(): continue
            data['record.$.'+name_key+'.'+key+'.for'] += fixture[fixture_key]
            data['record.$.'+name_key+'.'+key+'.against'] += fixture[opp_key + '_team.' + key]
            data['record.$.'+'total'+'.'+key+'.for'] += fixture[fixture_key]
            data['record.$.'+'total'+'.'+key+'.against'] += fixture[opp_key + '_team.' + key]

    if fixture['result'] == name_key[0].upper():
        data['record.$.' + name_key + '.won'] += 1
        data['record.$.' + 'total' + '.won'] += 1
        data['record.$.' + name_key + '.points'] += 3
        data['record.$.' + 'total' + '.points'] += 3

    elif fixture['result'] == 'D':
        data['record.$.' + name_key + '.drawn'] += 1
        data['record.$.' + 'total' + '.drawn'] += 1
        data['record.$.' + name_key + '.points'] += 1
        data['record.$.' + 'total' + '.points'] += 1

    else:
        data['record.$.' + name_key + '.lost'] += 1
        data['record.$.' + 'total' + '.lost'] += 1


def update_ref(ref, fixture):

    if 'record.$.season.gid' not in ref.keys():

        ref.update({"record.$.season.gid":fixture['season.gid'],
                    "record.$.competition.gid": fixture['competition.gid'],
                    "record.$.season.name.fdata_name":fixture['season.name.fdata_name'],
                    "record.$.cards.yellow":0,
                    "record.$.cards.red":0,
                    "record.$.fouls":0,
                    "name.fdata_name":fixture['referee.name.fdata_name'],
                    "gid":fixture['referee.gid']
                    })

    for team in ['home_team.', 'away_team.']:
        for key in ['cards.yellow', 'cards.red', 'fouls']:
            if (team + key) not in fixture.keys() or (fixture[team + key ] is None): continue
            ref['record.$.' + key] += fixture[team + key ]



def update_part(leg, fixture, isRound):

    if 'gid' not in leg.keys():

        if isRound:
            leg.update({"season.name.fdata_name": fixture['season.name.fdata_name'],
                    "season.gid":fixture['season.gid'],
                    "fixtures": [],
                    "gid": fixture['round.gid'],
                    "name.fdata_name": fixture['round.name.fdata_name']})
        else:
            leg.update({
                    "fixtures": [],
                    "gid": fixture['season.gid'],
                    "name.fdata_name": fixture['season.name.fdata_name']})

        leg.update(initialize_record_field(None, None, None, False))

    for team in ['away', 'home']:
        for act_team in [team , 'total']:
            for field in ['fouls', 'corners', "goals.full_time",
                          "goals.half_time", "shots.on_target",
                          "shots.total","cards.yellow",
                          "cards.red"]:
                field_key =  team + '_team.' + field
                if field_key not in fixture.keys(): continue
                leg['record.' + act_team + '.' + field] += fixture[field_key]

    if fixture['result'] == 'A':
        leg['record.away.wins'] += 1
    if fixture['result'] == 'H':
        leg['record.home.wins'] += 1
    if  fixture['result'] == 'D':
        leg['record.total.draws'] += 1


def get_ids_and_names(persister, round_index, teams_dict, refs_dict,
                      row, comp_id, seas_name, seas_id, fgid):



        round_id, round_name = getRoundGID(seas_id, round_index)
        fgid_str = str(fgid) if fgid > 9 else '0'+str(fgid)
        fgid = int(str(round_id)+fgid_str)

        try:
            t1id = teams_dict[row['HomeTeam']]
        except:
            generate_id(persister, 'teams', teams_dict, row['HomeTeam'])
            t1id = teams_dict[row['HomeTeam']]

        try:
            t2id = teams_dict[row['AwayTeam']]
        except:
            generate_id(persister, 'teams', teams_dict, row['AwayTeam'])
            t2id = teams_dict[row['AwayTeam']]

        ref_id = None
        if 'Referee' in row.keys() and row['Referee']:
            try:
                ref_id = refs_dict[row['Referee']]
            except:
                generate_id(persister, 'referees', refs_dict, row['Referee'])
                ref_id = refs_dict[row['Referee']]


        additional_info = {"gid":fgid,
                           "season.gid":seas_id,
                           "competition.gid":comp_id,
                           "season.name.fdata_name":seas_name,
                           "round.name.fdata_name":round_name,
                           "round.gid":round_id,
                           "home_team.gid":t1id,
                           "away_team.gid":t2id
                           }
        if ref_id:
            additional_info['referee.gid'] = ref_id

        return additional_info


def add_refs_and_teams_to_database(persister, teams, refs):


    for team in teams:
        rec_dict = {"record.competition.gid":teams[team]['record.$.competition.gid'],
                    "record.season.gid":teams[team]['record.$.season.gid'],
                    "gid":teams[team]['gid']}

        if persister.db['teams'].find_one(rec_dict) is None:
            persister.db['teams'].update({"gid":teams[team]['gid']},
                                         {"$push":{"record":{"competition":{"gid":teams[team]['record.$.competition.gid']},
                                                             "season":{"gid":teams[team]['record.$.season.gid']}}}}, upsert=True)

        persister.db['teams'].update(rec_dict,
                                     {"$set":teams[team]})


    for ref in refs:
        rec_dict = {"record.competition.gid":refs[ref]['record.$.competition.gid'],
                    "record.season.gid":refs[ref]['record.$.season.gid'],
                    "gid":refs[ref]['gid']}

        if persister.db['refs'].find_one(rec_dict) is None:
            persister.db['refs'].update({"gid":refs[ref]['gid']},
                                         {"$push":{"record":{"competition":{"gid":teams[team]['record.$.competition.gid']},
                                                             "season":{"gid":teams[team]['record.$.season.gid']}}}}, upsert=True)

        persister.db['refs'].update(rec_dict,{"$set":refs[ref]})
