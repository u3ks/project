# mapping fixture statistics and data to database fields

fixture_maping = { 'Date' : 'match_date', 'HomeTeam': 'home_team.name.fdata_name',
                'AwayTeam': 'away_team.name.fdata_name', 'FTHG': 'home_team.goals.full_time',
                'FTAG': 'away_team.goals.full_time', 'FTR': 'result',
                'HTHG': 'home_team.goals.half_time',
                'HTAG': 'away_team.goals.half_time', 'Referee': 'referee.name.fdata_name',
                'HS': 'home_team.shots.total', 'HST': 'home_team.shots.on_target',
                'HHW': 'home_team.shots.woodwork', 'HC': 'home_team.corners',
                'HF': 'home_team.fouls', 'HO': 'home_team.offsides',
                'HY': 'home_team.cards.yellow', 'HR': 'home_team.cards.red',
                'AS': 'away_team.shots.total', 'AST': 'away_team.shots.on_target',
                'AHW': 'away_team.shots.woodwork', 'AC': 'away_team.corners',
                'AF': 'away_team.fouls', 'AO': 'away_team.offsides',
                'AY': 'away_team.cards.yellow', 'AR': 'away_team.cards.red'}


#map fdata codes to odds fields
odds_mapping = {'B365H': 'bet365.home_win', 'VCH': 'vc_bet.home_win',
              'LBD': 'ladbrokes.draw', 'BWD': 'bet_win.draw',
              'SYD': 'stanleybet.draw', 'LBH': 'ladbrokes.home_win',
              'IWA': 'interwetten.away_win', 'SOA': 'sporting_odds.away_win',
              'SBH': 'sportingbet.home_win', 'SJH': 'stan_james.home_win',
              'PSA': 'pinnacle_sports.away_win', 'BSA': 'blue_square.away_win',
              'IWH': 'interwetten.home_win', 'PSD': 'pinnacle_sports.draw',
              'B365A': 'bet365.away_win', 'GBA': 'gamebookers.away_win',
              'SYH': 'stanleybet.home_win', 'SYA': 'stanleybet.away_win',
              'SJA': 'stan_james.away_win','VCD': 'vc_bet.draw', 'BWA': 'bet_win.away_win',
              'SBD': 'sportingbet.draw', 'BSH': 'blue_square.home_win',
              'GBH': 'gamebookers.home_win', 'IWD': 'interwetten.draw',
              'PSH': 'pinnacle_sports.home_win', 'WHD': 'william_hill.draw',
              'BSD': 'blue_square.draw', 'SOD': 'sporting_odds.draw',
              'SOH': 'sporting_odds.home_win', 'WHA': 'william_hill.away_win',
               'VCA': 'vc_bet.away_win', 'GBD': 'gamebookers.draw',
               'WHH': 'william_hill.home_win',
               'B365D': 'bet365.draw', 'SJD': 'stan_james.draw',
               'SBA': 'sportingbet.away_win', 'LBA': 'ladbrokes.away_win',
               'BWH': 'bet_win.home_win',
               'HomeTeam':'home_team.name.fdata_name',
               'AwayTeam':'away_team.name.fdata_name',
               'Date' : 'match_date',
               "GBAHH":'gamebookers.asian_home',
               "GBAHA":'gamebookers.asian_away',
               "GBAH":'gamebookers.asian_size',
               "LBAHH":'ladbrokes.asian_home',
               "LBAHA":'ladbrokes.asian_away',
               "LBAH":'ladbrokes.asian_size',
               "B365AHH":'bet365.asian_home',
               "B365AHA":'bet365.asian_away',
               "B365AH":'bet365.asian_size',
               "GB>2.5":'gamebookers.over25',
               "GB<2.5":'gamebookers.under25',
               "B365>2.5":'bet365.over25',
               "B365<2.5":'bet365.under25'
               }

# home, away, total record skeleton

initial_record_team_subfields =    {"cards.yellow.for":0,
                                    "cards.yellow.against":0,
                                    "cards.red.for":0,
                                    "cards.red.against":0,

                                    "fouls.for":0,
                                    "fouls.against":0,

                                    "corners.for":0,
                                    "corners.against":0,

                                    "goals.full_time.for":0,
                                    "goals.full_time.against":0,
                                    "goals.half_time.for":0,
                                    "goals.half_time.against":0,

                                    "shots.on_target.for":0,
                                    "shots.on_target.against":0,
                                    "shots.total.for":0,
                                    "shots.total.against":0,

                                    "won":0,
                                    "lost":0,
                                    "drawn":0,
                                    "points":0
                                    }

# round, season skeleton
initial_record_subfields =         {
                                    "fouls": 0, "corners": 0,
                                    "goals.full_time": 0, "goals.half_time":0,
                                    "shots.on_target":0, "shots.total":0,
                                    "cards.yellow":0, "cards.red": 0,
                                    }

def initialize_record_field(seasid, compid, seasname, team=True):

    '''

    Return a record field skeleton for a team or round or season.

    '''

    if team:

        team_record = {"record.$.season.gid":seasid,
                       "record.$.season.name.fdata_name":seasname,
                       "record.$.competition.gid":compid}

        for key in ['home', 'away', 'total']:
            for each in initial_record_team_subfields.keys():
                team_record['record.$.'+ key + '.'+ each] = 0

        return team_record

    else:
        team_record = {}
        for each in initial_record_subfields.keys():
            for key in ['home', 'away', 'total']:
                team_record['record.'+ key + '.'+ each] = 0
        team_record['record.total.draws'] = 0
        team_record['record.home.wins'] = 0
        team_record['record.away.wins'] = 0

        return team_record