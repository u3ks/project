'''

Script that sets the variables used to make ids for the teams and refs.

'''
from utilities.database import MongoPersister

id_generator = {"teams":1, "referees":1}


if __name__ == '__main__':

    persister = MongoPersister('db.prediction_model')
    persister.connect()
    persister.db['id_generator'].insert(id_generator)
    persister.close()