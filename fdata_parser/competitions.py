"""

    Dictionary that holds the basic information about the competitions that are
going to be parsed and when ran as a script adds that information
to the database.

"""

from utilities.database import MongoPersister

comps = [

         {'country': {'fdata_name': 'Greece'},
          'fdata_code': 'G1',
          'gid': 11,
          'name': {'fdata_name': 'Ethniki Katigoria'}},


         {'country': {'fdata_name': 'Italy'},
          'fdata_code': 'I1',
          'fdata_stats': 2005,
          'gid': 12,
          'name': {'fdata_name': 'Serie A'}},


         {'country': {'fdata_name': 'Italy'},
          'fdata_code': 'I2',
          'gid': 13,
          'name': {'fdata_name': 'Serie B'}},


         {'country': {'fdata_name': 'England'},
          'fdata_code': 'E1',
          'fdata_stats': 2004,
          'gid': 14,
          'name': {'fdata_name': 'Championship'}},


         {'country': {'fdata_name': 'England'},
          'fdata_code': 'E0',
          'fdata_stats': 2000,
          'gid': 15,
          'name': {'fdata_name': 'Premiership'}},


         {'country': {'fdata_name': 'England'},
          'fdata_code': 'E3',
          'fdata_stats': 2004,
          'gid': 16,
          'name': {'fdata_name': 'League Two'}},


         {'country': {'fdata_name': 'England'},
          'fdata_code': 'E2',
          'fdata_stats': 2004,
          'gid': 17,
          'name': {'fdata_name': 'League One'}},


         {'country': {'fdata_name': 'Portugal'},
          'fdata_code': 'P1',
          'gid': 18,
          'name': {'fdata_name': 'Liga I'}},
         {'country': {'fdata_name': 'Spain'},


          'fdata_code': 'SP1',
          'fdata_stats': 2005,
          'gid': 19,
          'name': {'fdata_name': 'La Liga Primera Division'}},


         {'country': {'fdata_name': 'Spain'},
          'fdata_code': 'SP2',
          'gid': 20,
          'name': {'fdata_name': 'La Liga Segunda Division'}},


         {'country': {'fdata_name': 'England'},
          'fdata_code': 'EC',
          'fdata_stats': 2005,
          'gid': 21,
          'name': {'fdata_name': 'Conference'}},


         {'country': {'fdata_name': 'Turkey'},
          'fdata_code': 'T1',
          'gid': 22,
          'name': {'fdata_name': 'Futbol Ligi 1'}},


         {'country': {'fdata_name': 'Netherlands'},
          'fdata_code': 'N1',
          'gid': 23,
          'name': {'fdata_name': 'Eredivisie'}},


         {'country': {'fdata_name': 'Scotland'},
          'fdata_code': 'SC1',
          'gid': 24,
          'name': {'fdata_name': 'Division One'}},


         {'country': {'fdata_name': 'Scotland'},
          'fdata_code': 'SC0',
          'fdata_stats': 2000,
          'gid': 25,
          'name': {'fdata_name': 'Premier League'}},


         {'country': {'fdata_name': 'Scotland'},
          'fdata_code': 'SC3',
          'gid': 26,
          'name': {'fdata_name': 'Division Three'}},


         {'country': {'fdata_name': 'Scotland'},
          'fdata_code': 'SC2',
          'gid': 27,
          'name': {'fdata_name': 'Division Two'}},


         {'country': {'fdata_name': 'France'},
          'fdata_code': 'F1',
          'fdata_stats': 2005,
          'gid': 28,
          'name': {'fdata_name': 'Le Championnat'}},


         {'country': {'fdata_name': 'France'},
          'fdata_code': 'F2',
          'gid': 29,
          'name': {'fdata_name': 'Division 2'}},


         {'country': {'fdata_name': 'Belgium'},
          'fdata_code': 'B1',
          'gid': 30,
          'name': {'fdata_name': 'Jupiler League'}},


         {'country': {'fdata_name': 'Germany'},
          'fdata_code': 'D2',
          'gid': 31,
          'name': {'fdata_name': 'Bundesliga 2'}},


         {'country': {'fdata_name': 'Germany'},
          'fdata_code': 'D1',
          'fdata_stats': 2005,
          'gid': 32,
          'name': {'fdata_name': 'Bundesliga'}}

        ]

if __name__ == '__main__':

    persister = MongoPersister('db.prediction_model')
    persister.connect()
    for comp in comps:
        persister.db['competitions'].update({"gid":comp['gid']},
                                            comp,
                                            upsert=True)
    persister.close()
