from Tkinter import *
from ttk import Frame, Button, Label, Style
from ttk import Entry
# from events import *

events = ['Goal', 'Attempt', 'Chance created', 'Run', 'Intervention']

class Example(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)

        self.parent = parent
        self._team1 = []
        self._team2 = []
        self.initUI()

    def initUI(self):

        self.parent.title("Collector")

        self.columnconfigure(0, pad=3)
        self.columnconfigure(1, pad=3)
        self.columnconfigure(2, pad=3)
        self.columnconfigure(3, pad=3)

        self.rowconfigure(0, pad=3)
        self.rowconfigure(1, pad=3)
        self.rowconfigure(2, pad=3)
        self.rowconfigure(3, pad=3)
        self.rowconfigure(4, pad=3)
        self.rowconfigure(5, pad=3)

        self.resetUI()
        self.addButtons()

        self.pack()


    def resetUI(self):
        self.initCategory()
        self.initType()
        self.initPlayer()
        self.initTime()
        self.initTeam()

    def initTeam(self):

        cat = Label(self, text='Team')
        cat.grid(row=0, column=0)
        self._team = Entry(self)
        self._team.grid(row=0, column=1)

    def initCategory(self):
        # the category
        cat = Label(self, text='Category')
        cat.grid(row=1, column=0)
        self._cat = StringVar(self)
        self._cat.set("Goal") # initial value
        option = apply(OptionMenu, (self, self._cat) + tuple(events))
        option.grid(row=1, column=1, columnspan=2)


    def initType(self):
        # the category
        typeL = Label(self, text='Type')
        typeL.grid(row=2, column=0)
        self._type = StringVar(self)
        self._type.set("Good attempt") # initial value
        option = OptionMenu(self, self._type, 'Penalty', 'Solo', 'One on one',
                            'Corner', 'Direct Free kick', 'Good attempt',
                            'Poor Attempt', 'Attempt')
        option.grid(row=2, column=1, columnspan=2)


    def initPlayer(self):
        # the category
        pl = Label(self, text='Player')
        pl.grid(row=3, column=0)
        self._pl = Entry(self)
        self._pl.grid(row=3, column=1,columnspan=2, sticky=W+E)


    def initTime(self):
        # the category
        pl = Label(self, text='Time')
        pl.grid(row=4, column=0)
        self._time = Entry(self)
        self._time.grid(row=4, column=1)


    def addButtons(self):

        def ok():

            team = self._team1 if self._team == 'team1' else self._team2
            event = {"category":self._cat.get(), 'team1': self._team.get(),
                     'player': self._pl.get(), 'time': self._time.get(),
                     'type': self._type.get()}
            team.append(event)
            self.resetUI()

        def save_to_file():
            with open("events.txt", 'w') as f:
                f.write(self.mkstr(self._team1))
                f.write(self.mkstr(self._team2))

        recB = Button(self, text="Record", command=ok)
        recB.grid(row=6, column = 1)

        recB = Button(self, text="Save", command=save_to_file)
        recB.grid(row=6, column = 3)

    def mkstr(self, arr):
        rs = '['
        for ls in arr:
            rs += ' { '
            for a in ls.keys(): rs += a + ': ' + ls[a] + ','
            rs += ' }, '
        return rs + ' ]\n'

def main():

    root = Tk()
    app = Example(root)
    root.mainloop()


if __name__ == '__main__':
    main()