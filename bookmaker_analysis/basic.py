'''

Analysis of bookmaker odds.

Goals:

    - to find average bookmaker turnover for available markets

    - to find where bookmaker has smallest advantage

    - to find possible systematic preferences in odds

    -

'''

from utilities.database import MongoPersister
from utilities.parser import convert_data

def bookie_turnover(bookie, date=None):

    query = {bookie:{"$exists":1}}
    date = convert_data(date)
    print type(date)
    if date:  query.update({"match_date":{"$gte":date}})

    persister = MongoPersister('db.prediction_model')
    persister.connect()

    odds = persister.db['odds'].find(query, {bookie:1}).sort("match_date", 1)   # find all odds where the bookmaker exists
    N = odds.count()
    print 'Retrieved %d documents'%N
    over_under_odds = 0
    singles_average = 0
    average_overUnder = 0
    average_asian = 0
    asian_odds = 0

    for odd in odds:
        # count all the singles
        singles_average += (100/odd[bookie]['home_win'] + 100/odd[bookie]['draw'] + 100/odd[bookie]['away_win'])

        #if there is over and under information take it into account
        if 'under25' in odd[bookie].keys() and odd[bookie]['under25']:
            over_under_odds += 1
            average_overUnder += (100/odd[bookie]['under25'] + 100/odd[bookie]['over25'])

        # if there are asian handicaps take their average as well
        if 'asian_size' in odd[bookie].keys() and odd[bookie]['asian_size']:
            asian_odds += 1
            average_asian += (100/odd[bookie]['asian_home'] + 100/odd[bookie]['asian_away'])

    #print results in percents
    print 'Singles average turnover: ', singles_average / float(N), '(based on %d odds)'%N
    print 'Over/Under average turnover: ', average_overUnder / float(over_under_odds), '(based on %d odds)'%asian_odds
    print 'Asian Handicap average turnover: ', average_asian / float(asian_odds), '(based on %d odds)'%asian_odds


    persister.close()


if __name__ == '__main__':
    bookie_turnover('bet365', '1/8/14')