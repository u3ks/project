'''
Created on 16.11.2014

@author: User
'''

def goals():

    fields = {"home_team.goals.full_time":1, "away_team.goals.full_time":1}
    query = {}
    doc_key = 'goals.full_time'

    return query, fields, doc_key

def shots():

    fields = {"home_team.shots.total":1, "away_team.shots.total":1}
    query = {}
    doc_key = 'shots.total'

    return query, fields, doc_key

def red_cards():

    fields = {"home_team.cards.red":1, "away_team.cards.red":1}
    query = {}
    doc_key = 'cards.red'

    return query, fields, doc_key

def shots_on_target():

    fields = {"home_team.shots.on_target":1, "away_team.shots.on_target":1}
    query = {}
    doc_key = 'shots.on_target'

    return query, fields, doc_key