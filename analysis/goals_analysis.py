'''

This module has functions to :
 - count the occurrences of stats and compare them to the poisson distribution,
   used by the model.

'''

from utilities.database import MongoPersister
import scipy.stats
from matplotlib import pyplot as plt
import numpy as np
from helper import *

# scipy.stats.poisson.pmf(i, h_prob)


def get_stat_chart(conn, query, fields, doc_field):


    home, away, total = get_goals(conn, query, fields, doc_field)

    total = away
    specific_goal_modes = []
    diff_values = list(np.unique(total))
    for i in range(0, len(diff_values)):
        specific_goal_modes.append(total.count(i))
    mean = np.mean(total)
    poiss = get_poisson_goal_distribution(mean, len(total), len(diff_values))
    stat = doc_field.split('.')[0] if doc_field.find('.') >= 0 else doc_field
    stat = stat.capitalize()
    create_goals_bar_chart(specific_goal_modes, poiss,
                           diff_values, stat, mean)


def get_goals(conn, query, fields, doc_field):
    '''
    Returns the home, away and total goals for each game
    that gets picked up by the query.
    @param conn: the database connection
    @param seasons: the mongo query for the fixtures collection
    '''

    home = []
    away = []

    fixtures = conn.db['fixtures'].find(query,
                                        fields)
    # record the actual goals
    for fx in fixtures:

        if doc_field.find('.') >= 0:

            pf = doc_field.split('.')[0]
            sf = doc_field.split('.')[1]
            home.append(fx['home_team'][pf][sf])
            away.append(fx['away_team'][pf][sf])

        else:
            home.append(fx['home_team'][doc_field])
            away.append(fx['away_team'][doc_field])

    return home, away, map(lambda i:home[i] + away[i], range(0, len(home)))


def create_goals_bar_chart(db_goals, poisson_goals,
                           diff_values, stat, mean):



    n_groups = len(diff_values)
    fig, ax = plt.subplots()

    index = np.arange(n_groups)
    bar_width = 0.35

    opacity = 0.4
    error_config = {'ecolor': '0.3'}

    rects1 = plt.bar(index, db_goals, bar_width,
                     alpha=opacity,
                     color='b',
                     error_kw=error_config,
                     label='Actual')

    rects2 = plt.bar(index + bar_width, poisson_goals, bar_width,
                     alpha=opacity,
                     color='r',
                     error_kw=error_config,
                     label='Poisson')

    plt.axvline(mean, color='b', linestyle='dashed', linewidth=2)

    plt.xlabel(stat)
    plt.ylabel('Number of ' + stat)
    plt.title('Actual %s vs Poisson predicted'%stat)
    plt.xticks(index + bar_width, diff_values)
    plt.legend()

    plt.tight_layout()
    plt.show()


def get_poisson_goal_distribution(mean, games, values):

    return [scipy.stats.poisson.pmf(i, mean) * games
            for i in range(0, values)]


if __name__ == '__main__':
    conn = MongoPersister('db.prediction_model')
    conn.connect()

    get_stat_chart(conn, *goals())

    conn.close()
