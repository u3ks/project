from ConfigParser import ConfigParser
import os
from pymongo import MongoClient


class MongoPersister(object):


    def __init__(self, db):

        filepath = os.path.dirname(os.path.realpath(__file__)) + '/local.ini'
        self.__config_parser = ConfigParser()
        self.__config_parser.read(filepath)
        self.__mongourl = self.__config_parser.get('mongo', db)
        self._client = None

    def connect(self):
        self._client = MongoClient(self.__mongourl, tz_aware=True)


    @property
    def client(self):
        return self._client


    @property
    def db(self):
        return self.client.get_default_database()


    def close(self):
        self.client.close()
