'''
Created on 27.06.2014

@author: User
'''
from datetime import datetime
from database import MongoPersister
from stratagemgsmparser import parser_utilities
import re

date_formats_sources= ['football-data']
date_formats = ['%d/%m/%y']

date_format = "%Y-%m-%dT%H:%M:%SZ"


def convert_data(value):

    if not value:
        return None

    ref_value = None

    #if fails value is unicode name
    try:
        ref_value = str(value)
    except:
        return unicode(value)

    #try to cast to integer
    try:
        ref_value = int(ref_value)
        return ref_value
    except:
        ref_value = str(value)

    #try to convert to float
    try:
        ref_value = float(ref_value)
        return ref_value
    except:
        ref_value = str(value)

    #try to cast as datetime
    try:
        ref_value = __parse_date(ref_value)
        return ref_value
    except:
        ref_value = str(value)

    #return as string
    return ref_value.encode('utf8')


def __parse_date(value):

    for possible_format in date_formats:
        try:
            parse_time = datetime.strptime(value, possible_format)
            parse_time = parse_time.strftime(date_format)
            return datetime.strptime(parse_time, date_format)
        except: continue
    raise Exception('Value not date')
