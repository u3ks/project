'''
Created on 30.07.2014

@author: User
'''
import math

def median(arr):

    return arr[len(arr)/2]


def mean (arr):
    '''
    Compute the mean of an array in single pass.
    '''
    sum = 0
    for entry in arr:
        sum += entry
    return (sum/float(len(arr)))


def factorial(i, permutations=0):
    '''
    Compute permutations or factorial if permutations is 0
    '''

    if permutations == 0:
        permutations = i
    sum = 1
    n = 1
    while n <= permutations:
        sum *= i
        i -= 1
        n += 1
    return sum


def st_dev(arr):
    '''
    Compute the standard deviation of an array in single pass.
    '''
    N = float(len(arr))
    sum = 0
    sum_squared = 0
    for entry in arr:
        sum += entry
        sum_squared += entry**2
    return math.sqrt((sum_squared/N) - (sum**2/N**2))


def st_score(x, arr):
    '''
    Compute the standard score of X in an array.
    '''
    return (x - mean(arr)) / st_dev(arr)


def get_deviations(arr, mean):

    return [(x-mean)**2 for x in arr]


def clear_with_quartiles(arr):
    '''
    Clear the extreme values in the data.
    '''

    arr.sort()
    median_index = len(arr)/2
    lower_quartile = median_index/2
    upper_quartile = median_index + lower_quartile + 1
    return arr[lower_quartile:upper_quartile+1]


def combinatorics(pos, outcomes):
    '''
    Return the successfull number of combinations.
    '''
    return factorial(pos, outcomes)/factorial(outcomes)


def equal(a, b):
    '''
    Compare two floats relatively.
    '''
    return abs(a - b) < 1e-6


def linear_regression(data):
    '''
    Return a and b used in the formula y 'y = bx + a' to plot
    linear regression.
    '''

    xs = [entry['x'] for entry in data]
    ys = [entry['y'] for entry in data]
    mean_x = mean(xs)
    mean_y = mean(ys)
    s = 0
    ss = 0
    for i in range(0, len(xs)):
        s += (xs[i] - mean_x)*(ys[i] - mean_y)
        ss += (xs[i] - mean_x)**2
    b = s/ss
    a = mean_y - (b*mean_x)
    return a,b


def correlation_coefficient(data, xs = None, ys = None):

    if xs is None and ys is None:
        xs = [entry['x'] for entry in data]
        ys = [entry['y'] for entry in data]

    mean_x = mean(xs)
    mean_y = mean(ys)
    s = 0
    sx = 0
    sy = 0
    for i in range(0, len(xs)):
        s += (xs[i] - mean_x)*(ys[i] - mean_y)
        sx += (xs[i] - mean_x)**2
        sy += (ys[i] - mean_y)**2
    return s / (math.sqrt(sx*sy))


if __name__ == '__main__':
    print correlation_coefficient([{"x" : 3, "y": 8}, {"x" : 4, "y": 5},
                             {"x" : 5, "y": 2}])
