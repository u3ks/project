'''
Created on 01.07.2014

@author: User
'''
from utilities.database import MongoPersister
from football_data.footballdataanalysers import EloPredictor, clear_unanalyzed_fixtures, update_elo_ratings, GeteasonStats
from football_data.footbaldataparser import FootballDataParser

odds = ['B365H', 'B365D', 'B365A', 'BSH', 'BSD', 'BSA',
        'BWH', 'BWD', 'BWA', 'GBH', 'GBD', 'GBA', 'IWH',
        'IWD', 'IWA', 'LBH', 'LBD', 'LBA', 'PSH', 'PSD', 'PSA',
        'SOH', 'SOD', 'SOA', 'SBH', 'SBD', 'SBA', 'SJH', 'SJD', 'SJA',
        'SYH', 'SYD', 'SYA', 'VCH', 'VCD', 'VCA', 'WHH', 'WHD', 'WHA']

odds_codes = {'B365H': 'bet365.home_win', 'VCH': 'vc_bet.home_win',
              'LBD': 'ladbrokes.draw', 'BWD': 'bet_win.draw',
              'SYD': 'stanleybet.draw', 'LBH': 'ladbrokes.home_win',
              'IWA': 'interwetten.away_win', 'SOA': 'sporting_odds_.away_win',
              'SBH': 'sportingbet.home_win', 'SJH': 'stan_james.home_win',
              'PSA': 'pinnacle_sports.away_win', 'BSA': 'blue_square.away_win',
              'IWH': 'interwetten.home_win', 'PSD': 'pinnacle_sports.draw',
              'B365A': 'bet365.away_win', 'GBA': 'gamebookers.away_win',
              'SYH': 'stanleybet.home_win', 'SYA': 'stanleybet.away_win',
              'SJA': 'stan_james.away_win','VCD': 'vc_bet.draw', 'BWA': 'bet_win.away_win',
              'SBD': 'sportingbet.draw', 'BSH': 'blue_square.home_win',
              'GBH': 'gamebookers.home_win', 'IWD': 'interwetten.draw',
              'PSH': 'pinnacle_sports.home_win', 'WHD': 'william_hill.draw',
              'BSD': 'blue_square.draw', 'SOD': 'sporting_odds_.draw',
              'SOH': 'sporting_odds_.home_win', 'WHA': 'william_hill.away_win',
               'VCA': 'vc_bet.away_win', 'GBD': 'gamebookers.draw',
               'WHH': 'william_hill.home_win',
               'B365D': 'bet365.draw', 'SJD': 'stan_james.draw',
               'SBA': 'sportingbet.away_win', 'LBA': 'ladbrokes.away_win',
               'BWH': 'bet_win.home_win'}


def split_season_file_to_rounds(filepath,comp):

    csv_file = open(filepath, 'r')
    lines = csv_file.readlines()
    header = lines[0]
    del lines[0]
    i = 0
    r = 1
    while i <= 380:
        round = lines[i:i+10]
        round.insert(0, header)
        round_file = open("../football_data/data/rounds/%sR%d.csv"%(comp,r), 'w')
        for a in round:
            round_file.write(a)
        round_file.close()
        r+=1
        i+=10


def exctract_odds_and_fixtures_from_fd_csv_file(filepath):

    csv_file = open(filepath, 'r')
    lines = csv_file.readlines()
    header = lines[0]
    header = header.split(',')
    del lines[0]
    odds_pos = []
    fixtures_odds = []
    for i in range(0, len(odds)):
        try:
            odds_pos.append(header.index(odds[i]))
            fixtures_odds.append(odds[i])
        except:
            continue

    date_pos = header.index('Date')
    season = structure_season_name(lines[0].split(',')[date_pos])
    fixtures  = []
    persister = MongoPersister('db.test_model')
    persister.connect()
    for line_comma in lines:
        line = line_comma.split(',')
        if line[0] != 'E0': continue
        t1 = line[2]
        t2 = line[3]
        fid = "%s-%s-%s"%(season,t1,t2)
        specific_odds = {}
        for i in range(0,len(fixtures_odds)):
            odd = line[odds_pos[i]]
            try:
                odd = float(odd)
            except:
                continue
            specific_odds[odds_codes[fixtures_odds[i]]] = odd
        persister.db['bookie_odds'].update({"fid":fid},
                                   {"$set":specific_odds},upsert=True)
        string = "%s/%s\n"%(t1,t2)
        fixtures.append(string)

    persister.close()
    return season, fixtures


def structure_season_name(date):
    year_adj = 0
    date = date.split('/')
    month = int(date[1])
    year = int(date[2])
    if month >= 8 and month <= 12:
        year_adj = 1
    elif month >= 1 and month <= 5:
        year_adj = -1
    return str(year) + '/' + str(year + year_adj)


def simulate_season_betting(future_fs, season, comp):

    bank_roll = 200
    future_fixtures = open(future_fs,'r')
    lines = future_fixtures.readlines()
    persister = MongoPersister('db.test_model')
    persister.connect()
    i = 60
    all_bets = []
    while i < 380:
        round_matches = lines[i:i+10] #for each round
        bets = []
        for match in round_matches:
            teams = match.split('/')
            teams[1] = teams[1].replace('\n','')
            predictor = EloPredictor(teams[0],teams[1], season)
            predictor.predict() #make prediction for each match
            fid = '%s-%s-%s'%(season,teams[0],teams[1])
            bookie_odds = persister.db['bookie_odds'].find_one({"fid":fid})['bet365']
            predicted_odds = persister.db['predictions'].find_one({"fid":fid})['predicted_odds']
            for key in bookie_odds.keys():
                if bookie_odds[key] > predicted_odds[key]:
                    if 100/predicted_odds[key] >= 35.0:
                        bank_roll = bank_roll - 5
                        if bank_roll < 0:
                            print bank_roll
                        all_bets.append([fid,key,bookie_odds[key],predicted_odds[key]])
                        bets.append([fid,key,bookie_odds[key]])

        round_num = i/10+1
        p = FootballDataParser('../football_data/data/rounds/%sR%d.csv'%(comp,round_num))
        p.parse()
        s = GeteasonStats(season)
        s.getTeamStats()
        update_elo_ratings(season)
        clear_unanalyzed_fixtures(season)

        for bet in bets:
            result = persister.db['fixtures'].find_one({"fid":bet[0]})['result']
            persister.db['fixtures'].update({"fid":bet[0]},
                                            {"$set":{
                                                     "placed.to_win": (5 * bet[2] - 5),
                                                     "placed.result": bet[1][0].upper(),
                                                     "placed.amount" : 5}})
            if result.lower() == bet[1][0].lower():
                bank_roll = bank_roll + bet[2] * 5
        i += 10
    print bank_roll
    for bet in all_bets: print bet


def add_rounds(lo, hi, season, comp):

    for i in range(lo, hi+1):
        p = FootballDataParser('../football_data/data/rounds/%sR%d.csv'%(comp,i))
        p.parse()
        s = GeteasonStats(season)
        s.getTeamStats()
        update_elo_ratings(season)
        clear_unanalyzed_fixtures([season])


def run():
#     exctract_odds_and_fixtures_from_fd_csv_file('../football_data/E014.csv',
#                                                 '../football_data/data/future_matches/ff.csv')
#     add_rounds(1, 6, "13/14", 'E0')

#     simulate_season_betting("../football_data/data/future_matches/ff.csv",
#                             "13/14", 'E0')
    exctract_odds_and_fixtures_from_fd_csv_file('../football_data/data/E012.csv',
                                                None)


if __name__ == '__main__':
    run()
