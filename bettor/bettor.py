from utilities.database import MongoPersister
from datetime import datetime, timedelta
from ratign import get_rating_diff
from prediction_analysis import compile_prediction_analysis_file
from pprint import pprint

def get_upcoming_and_show_value(betting_strategy, analysis, current_bankroll):

    persister = MongoPersister('db.prediction_model')
    persister.connect()
    date = datetime.now()
    available = persister.db['odds'].find({"match_date":{"$gte":date}},
                                          {"home_team":1,
                                            "away_team":1,
                                            "predicted_odds":1,
                                            "bet365":1,
                                            "_id":0})
    available = list(available)
    generate_documents = []
    print 'AVAILABLE BETS'
    for each in available:
        print '\n'
        pprint(each)


    print '\n\n\n'
    print 'VAlUE BETS ACCORDING TO STRATEGY "', betting_strategy, '" BASED ON ANALYSIS FROM "', analysis, '"\n'
    for each in available:
        bet = bet_strategy(each['bet365'], each['predicted_odds'],
                           'bet365', current_bankroll)
        if bet[0]:
            print 'Bet %d on %s for game %s - %s with %s'%(bet[4], bet[0],
                                                           each['home_team']['name']['fdata_name'],
                                                           each['away_team']['name']['fdata_name'],
                                                           bet[3]
                                                           )
            print 'Based on %s : %.2f from the predicted odds, against %s : %.2f from the bookmaker'%(bet[0], bet[2], bet[0], bet[1])
            print '-----------------------------'
            generate_documents.append('%s - %s - %d'%(each['home_team']['name']['fdata_name'],
                                                      each['away_team']['name']['fdata_name'],
                                                      1514))
    for game in generate_documents:
        compile_prediction_analysis_file(persister,game)
        get_rating_diff(game)

def bet_strategy(bookie_odds, predicted_odds, bookie, bank):

    best_bet = ['', 0, 0]
    for key in bookie_odds.keys():
        if bookie_odds[key] > predicted_odds[key] and 100/predicted_odds[key] >= 35.0:
            if predicted_odds[key] >= best_bet[2]:
                best_bet = [key, bookie_odds[key], predicted_odds[key], bookie, .02*bank]
    return best_bet


if __name__ == '__main__':
    get_upcoming_and_show_value('BASIC BETTING STRATEGY 1.0',
                                'BASIC POISSON ANALYSIS 1.0',
                                207.7)