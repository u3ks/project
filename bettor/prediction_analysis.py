from utilities.database import MongoPersister
from pprint import pprint
from datetime import datetime

def compile_prediction_analysis_file(persister, game):
    '''

    Get the prediction for the game, the teams' previous matches and their
    current statistics and fit them in a file.

    @param game: the game
    @param persister: the database connection
    '''

    home_team = game.split(' - ')[0]
    away_team = game.split(' - ')[1]
    season = int(game.split(' - ')[2])

    # get the prediction
    prediction = persister.db['odds'].find({"home_team.name.fdata_name":home_team,
                                            'away_team.name.fdata_name': away_team}
                                           ).sort('match_date', -1)[0]

    # get team statistics so far in the season
    home_team_stats = get_team_stats(persister, home_team, season)
    away_team_stats = get_team_stats(persister, away_team, season)


    # get the previous 6 games of the team, used to formulate the outcome chances
    home_team_previous = get_previous_games(persister, home_team,
                                            season, 'home')
    away_team_previous = get_previous_games(persister, away_team,
                                            season, 'away')
    # get the data in text format
    text = structure_text(prediction, home_team_stats, away_team_stats,
                          home_team_previous, away_team_previous)
    #write to file
    file_path = 'D:/Betting/1. RECORDS/2014 2015/' + home_team[:4] + away_team[:4] + '.txt'
    filex = open(file_path, 'w')
    filex.write(text)
    filex.close()

def get_team_stats(persister, team, season):

    '''
    Return the statistics for the current season for a specific team

    @param persister: database connection
    @param team: the team
    @param seaosn: the season
    @return: the team statistics
    '''

    return persister.db['teams'].find_one({"name.fdata_name": team,
                                           "record.season.gid": season},
                                          {"record.$": 1})


def get_previous_games(persister, team_name, season, team):

    ''''

    Get the last six games of a team, 3 in its current position(away, home)
    and three in the opposite.

    @param persister: database connection
    @param team_name: the name of the team
    @param seaoson: the seaosn id
    @param team: whether the team is host or guest
    @return: Cursor with the last 6 games
    '''

    opp_team = 'home' if team == 'away' else 'away'

    games1 = persister.db['fixtures'].find({"season.gid":season,
                                            team + '_team.name.fdata_name' : team_name}
                                           ).sort('match_date', -1).limit(3)

    games2 = persister.db['fixtures'].find({"season.gid":season,
                                            opp_team + '_team.name.fdata_name' : team_name}
                                           ).sort('match_date', -1).limit(3)

    return list(games1) + list(games2)


def structure_text(prediction, home_team_stats, away_team_stats,
                   home_team_previous, away_team_previous):
    ''''

    Format all the data in one string to be written to file.

    @param prediction: the prediction information
    @param home_team_stats: the home team statistics for this season
    @param away_team_stats: the away team statistics for this seaosn
    @param home_team_previous: the home team's previous 6 fixtures
    @param away_team_previous: the away team's previous 6 fixtures
    @return: A structured string containing information about the upcoming game

    '''
    sp = '       '
    ts = 60
    nl = '\n'
    sp2 = ' --- '

    home_team = prediction['home_team']['name']['fdata_name']
    away_team = prediction['away_team']['name']['fdata_name']

    # set up the header information
    header = get_middle(ts) + home_team + sp2 + away_team + nl + \
             get_middle(ts) + str(prediction['home_team']['rating']) + sp2 + \
             str(prediction['away_team']['rating']) + nl + \
             get_middle(ts) + sp + datetime.strftime(prediction['match_date'], '%d-%m-%Y')

    # set up the odds information
    odds = get_odds_text(prediction, sp, ts)

    #make the previous results section
    previous_matches = ''
    for i in range(0,len(home_team_previous)):
        ht = result_string(home_team_previous[i])
        previous_matches +=  ht + get_specifc_ts(len(ht), ts) + \
                            result_string(away_team_previous[i]) + nl

    #set up the team stats string
    stats = parse_team_stats(home_team_stats, away_team_stats, nl, ts)

    return header + '\n\n' + odds + '\n\n' + previous_matches + '\n\n' + stats


def result_string(game):

    ''''
    Structure the result of the game in a string.

    @param game: the game
    @return: a String containing the match result and date
    '''
    s = ' '
    match_date = datetime.strftime(game['match_date'], '%d-%m-%Y')
    home_team = game['home_team']['name']['fdata_name']
    away_team = game['away_team']['name']['fdata_name']
    home_score = game['home_team']['goals']['full_time']
    away_score = game['away_team']['goals']['full_time']

    return match_date + s + home_team + s + str(home_score) + ' - ' + \
           str(away_score) + s + away_team


def get_odds_text(prediction, sp, ts):

    '''

    Structure the prediction information and return a string.

    @param prediction: the prediction information
    @return: a string with the prediction info
    '''

    odds = 'Predicted odds : ' + str(prediction['predicted_odds']['home_win']) \
                + sp + str(prediction['predicted_odds']['draw']) + sp + \
                str(prediction['predicted_odds']['away_win'])

    odds += get_specifc_ts(len(odds), ts) + 'Bet365 odds    : ' + str(prediction['bet365']['home_win']) \
            + sp + str(prediction['bet365']['draw']) + sp + \
            str(prediction['bet365']['away_win'])

    return odds


def parse_team_stats(home_team_stats, away_team_stats, nl, ts):
    ''''

    Extract the team stats and save them to a string in the right format.

    @param home_team_stats: the home team statistics
    @param away_team_stats: the away team statistics
    @param nl: new line character
    @param ts: tab space character
    @return: a String with the stats info
    '''

    httd = home_team_stats['record'][0]['total']
    hthd = home_team_stats['record'][0]['home']
    atad = away_team_stats['record'][0]['away']
    attd = away_team_stats['record'][0]['total']

    total_stats = structure_specific_stats(httd, attd, 'TOTAL', ts)
    position_stats = structure_specific_stats(hthd, atad, 'SPECIFIC', ts)

    return total_stats + '\n\n\n' + position_stats


def structure_specific_stats(httd, attd, value, ts):

    stats_str = get_middle(ts) + value + '\n'

    for key in ['points', 'won',  'drawn',  'lost']:
        hts = '%s : %d'%(key.capitalize(), httd[key])
        ats = '%s : %d'%(key.capitalize(), attd[key])
        sts = get_specifc_ts(len(hts), ts)
        stats_str += '\n' + hts + sts + ats

    stats_str += '\n ----------------------'

    for key in ['goals', 'shots', 'cards']:

        for subkey in httd[key]:
            hts = '%s %s FOR : %d'%(key.capitalize(), subkey.capitalize(), httd[key][subkey]['for'])
            ats = '%s %s FOR : %d'%(key.capitalize(), subkey.capitalize(), attd[key][subkey]['for'])
            sts = get_specifc_ts(len(hts), ts)
            stats_str += '\n' + hts + sts + ats
            hts = '%s %s AGAINST : %d'%(key.capitalize(), subkey.capitalize(), httd[key][subkey]['against'])
            ats = '%s %s AGAINST : %d'%(key.capitalize(), subkey.capitalize(), attd[key][subkey]['against'])
            sts = get_specifc_ts(len(hts), ts)
    stats_str += '\n ----------------------'

    for key in ['fouls','corners']:
        hts = '%s FOR : %d'%(key.capitalize(), httd[key]['for'])
        ats = '%s FOR : %d'%(key.capitalize(), attd[key]['for'])
        sts = get_specifc_ts(len(hts), ts)
        stats_str += '\n' + hts + sts + ats
        hts = '%s AGAINST : %d'%(key.capitalize(), httd[key]['against'])
        ats = '%s AGAINST : %d'%(key.capitalize(), attd[key]['against'])
        sts = get_specifc_ts(len(hts), ts)
        stats_str += '\n' + hts + sts + ats

    return stats_str


def get_specifc_ts(str1len, ts):

    '''

    Get the specific number of spaces needed to create event columns.
    @param str1len: the length of the first column
    @param ts: the length of the distance
    @return: the space String
    '''
    ss = ' '
    return_str = ''
    for i in range(str1len, ts):
        return_str += ss

    return return_str


def get_middle(ts):

    '''

    Get the specific number of spaces needed to reach the middle.
    @param str1len: the length of the first column
    @param ts: the length of the distance
    @return: the space String
    '''
    ss = ' '
    return_str = ''
    for i in range(0, ts/2):
        return_str += ss

    return return_str


if __name__ == '__main__':

    persister = MongoPersister('db.prediction_model')
    persister.connect()

    for game in ['West Brom - Man United - 1514']:
        compile_prediction_analysis_file(persister, game)

    persister.close()
