'''
Created on 16.11.2014

@author: User
'''

from utilities.database import MongoPersister
from matplotlib import pyplot as plt
from pylab import rcParams

def get_rating_diff(game):

    t1 = game.split(' - ')[0]
    t2 = game.split(' - ')[1]
    seas_id = int(game.split(' - ')[2])

    rcParams['figure.figsize'] = 20, 10
    conn = MongoPersister('db.prediction_model')
    conn.connect()

    fields = {

              "home_team.name.fdata_name":1,
              "away_team.name.fdata_name":1,
              "away_team.rating.postgame":1,
              "home_team.rating.postgame":1,
              "round.name.fdata_name":1,
              "result":1
              }

    query = {"season.gid":seas_id, "$or": [{"home_team.name.fdata_name":t1},
                                           {"home_team.name.fdata_name":t2},
                                           {"away_team.name.fdata_name":t1},
                                           {"away_team.name.fdata_name":t2},
                                           ]}

    fixtures = conn.db['fixtures'].find(query, fields)

    t1_ratings = []
    t2_ratings = []
    times = []

    for fixture in fixtures:

        ratings = t1_ratings
        position = 'home_team'

        if opposition(fixture, position, times,
                      t1, t2, t1_ratings, t2_ratings):
            continue

        if t1 == fixture['away_team']['name']['fdata_name']:
            position = 'away_team'

        elif t2 == fixture['away_team']['name']['fdata_name']:
            ratings = t2_ratings
            position = 'away_team'

        elif t2 == fixture['home_team']['name']['fdata_name']:
            ratings = t2_ratings

        add_ratings(fixture, position, ratings, times)


    plot_chart(len(t1_ratings), [t1_ratings, t2_ratings], [t1, t2])
    conn.close()


def opposition(fixture, position, times,
               t1, t2, r1, r2):

    if t1 == fixture['home_team']['name']['fdata_name'] and t2 == fixture['away_team']['name']['fdata_name']:
        add_ratings(fixture, 'home_team', r1, times)
        add_ratings(fixture, 'away_team', r2, times)
        return True

    elif t2 == fixture['home_team']['name']['fdata_name'] and t1 == fixture['away_team']['name']['fdata_name']:
        add_ratings(fixture, 'home_team', r2, times)
        add_ratings(fixture, 'away_team', r1, times)
        return True

    return False



def add_ratings(fixture, position, ratings, times):

    opposition = 'home_team' if position == 'away_team' else 'away_team'
    data = {"rating": fixture[position]['rating']['postgame'],
            "vs": fixture[opposition]['name']['fdata_name'],
            "result":fixture['result'],
            "x": get_round(fixture['round']['name']['fdata_name'])}

    ratings.append(data)

    round_name = fixture['round']['name']['fdata_name']
    try: times.index(round_name)
    except: times.append(round_name)


def get_round(rname):
    if rname[1:3] in ['rd', 'th', 'nd', 'st']:
        return int(rname[0])
    else:
        return int(rname[0:2])



def plot_chart(timeframe, team_ratings, team_names):

    for i in range(len(team_ratings)):
        ratings = [t['rating'] for t in team_ratings[i]]
        plt.plot(range(1, timeframe+1), ratings , marker='o',
                 linestyle='--', label=team_names[i])
        for t in  team_ratings[i]:
            plt.text(t['x']-1, t['rating'], t['vs'], fontsize=9)

    plt.xlabel('Time')
    plt.ylabel('Team Rating')
    plt.title('Team Rating History')
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
       ncol=2)
    file_path = 'D:/Betting/1. RECORDS/2014 2015/' + team_names[0][:4] + team_names[1][:4] + '.png'
    plt.savefig(file_path,dpi=100)
    plt.clf()

if __name__ == '__main__':
    get_rating_diff('Liverpool - Man City - 1513')
