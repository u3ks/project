from pymongo import MongoClient

def profit_expectency(bookie_odds, stake_size, predicted_win_probability):

    return (stake_size * predicted_win_probability * (bookie_odds - 1)) - stake_size * ( 1 - predicted_win_probability)


def spe(bookie_odds, stake_size, predicted_win_probability):

    return (stake_size * bookie_odds)/predicted_win_probability - stake_size


def profit(starting_bank, end_bank):

    return end_bank - starting_bank


def cyield(profit, turnover):

    return profit / turnover


def bysn(sbank, fbank, syield, size, num):

    if not sbank:
        return fbank - (syield * size * (num/100))

    if not fbank:
        return sbank + (syield * size * (num/100))

    if not num:
        return ((fbank - sbank)*100)/(syield*size)

    if not syield:
        return (fbank - sbank)/((num/100)*size)

    if not size:
        return (fbank - sbank)/((num/100)*syield)


if __name__ == '__main__':
    print profit_expectency(5.35, 4.0, 1/4.80)