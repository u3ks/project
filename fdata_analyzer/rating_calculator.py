from utilities.database import MongoPersister
from fdata_analyzer.available_seasons import available_seasons

ELO_HOME_TEAM_RISK = 0.07
ELO_AWAY_TEAM_RISK = 0.05
ELO_GOALS_FROM_TOTAL_SHOTS = 9.7
ELO_GOALS_FROM_TARGET_SHOTS = 4.8

def calculate_elo_points(persister, all_seasons):


    if not all_seasons:
        parsable_seasons = [available_seasons[len(available_seasons)-1]]

    else:
        parsable_seasons = available_seasons

    for seas in parsable_seasons:

        fixs = persister.db['seasons'].find_one({"gid":seas}, {"fixtures":1})['fixtures']
        for f in fixs:

            fixture = persister.db['fixtures'].find_one({"gid":f['gid']})

            if 'analyzed' in fixture.keys():
                continue

            t1 = get_current_rating(persister, fixture['home_team']['gid'])
            t2 = get_current_rating(persister, fixture['away_team']['gid'])

            before_game1 = t1
            before_game2 = t2

            t1, t2 = calculate_elo(fixture,t1,t2)
            update_rating_in_database(persister, seas,
                                      fixture['home_team']['name']['fdata_name'],
                                      fixture['home_team']['gid'], t1)
            update_rating_in_database(persister, seas,
                                      fixture['away_team']['name']['fdata_name'],
                                      fixture['away_team']['gid'], t2)

            persister.db['fixtures'].update({"gid":fixture['gid']},
                                    {"$set":{
                                             "home_team.rating.pregame": before_game1,
                                             "home_team.rating.postgame": t1,
                                             "away_team.rating.pregame": before_game2,
                                             "away_team.rating.postgame": t2,
                                             "analyzed":True}})


def get_current_rating(persister, t1id):

    teamRating = persister.db['ratings'].find_one({"gid":t1id})
    if teamRating is None: return 78.0
    else: return teamRating['rating']


def update_rating_in_database(persister, seas, tname, tid, rating):

    persister.db['teams'].update({"gid":tid, "record.season.gid":seas}, {"$set":{'record.$.rating':rating}})
    persister.db['ratings'].update({"gid":tid},
                                   {"$set":{'rating':rating,
                                            "name.fdata_name":tname}}, upsert=True)

def calculate_elo(fixture, t1, t2):
    '''
    Get the points, which the teams bet for the game.
    Calculate each teams performance.
    If there is a winner:
    Determine how much better than the opposition he was,
    based on that value add the appropriate amount of points to
    the winner and loser.
    If there is no winner, split the pot.
    '''

    ht_bet = t1 * ELO_HOME_TEAM_RISK
    at_bet = t2 * ELO_AWAY_TEAM_RISK
    t1ratio,t2ratio = __calculate_ratio(fixture,t1,t2)
    t1 = t1 - ht_bet
    t2 = t2 - at_bet
    win = True if fixture['result'] != 'D' else False

    if win:
        winner = t2ratio
        loser = t1ratio
        if fixture['result'] == 'H':
            winner = t1ratio
            loser =  t2ratio
        difference = winner - loser if winner - loser > 0 else 0
        winner_plus_rating = (ht_bet + at_bet) * __point_table(win, difference)
        loser_plus_rating = (ht_bet + at_bet) - winner_plus_rating
        if fixture['result'] == 'H':
            t1 += winner_plus_rating
            t2 += loser_plus_rating
        else:
            t2 += winner_plus_rating
            t1 += loser_plus_rating
    else:
        t1 += (ht_bet + at_bet) / 2
        t2 += (ht_bet + at_bet) / 2

    return t1, t2


def __point_table(win, diff):

    if diff >= 0 and diff < 0.75: return 0.6
    if diff >= 0.75 and diff < 1.0: return 0.7
    if diff >= 1 and diff <= 2: return 0.78
    if diff >= 2.0 and diff < 2.5: return 0.85
    if diff >= 2.5: return 0.92


def __calculate_ratio(fixture, t1, t2):

    t1_goal_ratio = fixture['home_team']['goals']['full_time']
    t1_shot_ratio = float(fixture['home_team']['shots']['total']) / ELO_GOALS_FROM_TOTAL_SHOTS
    t1_hit_ratio = float(fixture['home_team']['shots']['on_target']) / ELO_GOALS_FROM_TARGET_SHOTS

    t2_goal_ratio = fixture['away_team']['goals']['full_time']
    t2_shot_ratio = float(fixture['away_team']['shots']['total']) / ELO_GOALS_FROM_TOTAL_SHOTS
    t2_hit_ratio = float(fixture['away_team']['shots']['on_target']) / ELO_GOALS_FROM_TARGET_SHOTS


    t1ratio = (t1_goal_ratio + t1_shot_ratio + t1_hit_ratio) / 3
    t2ratio = (t2_goal_ratio + t2_shot_ratio + t2_hit_ratio) / 3

    #factor in the difference between the teams' class
    t1ratio += -((t1-t2)/100)
    t2ratio += -((t2-t1)/100)

    #factor in home advantage
    t2ratio += t1/1000


    return t1ratio,t2ratio


def get_elo(seas):

    persister = MongoPersister('db.prediction_model')
    persister.connect()
    wanted = persister.db['teams'].find({"record.season.gid":seas}, {"name":1, "record.$.rating":1})
    teams = []
    for team in wanted:
        teams.append((team['name']['fdata_name'], team['record'][0]['rating']))
    teams = sorted(teams, key=lambda a: a[1],reverse=True)
    for index, team in enumerate(teams): print index+1, '. ', team
    persister.close()


if __name__ == '__main__':
    get_elo(1514)