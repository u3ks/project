import scipy.stats
import math

ELO_HOME_TEAM_RISK = 0.07
ELO_AWAY_TEAM_RISK = 0.05
ELO_GOALS_FROM_TOTAL_SHOTS = 9.7
ELO_GOALS_FROM_TARGET_SHOTS = 4.8

TEAM_GAMES_RELEVANCE = [0.4, 0.2, 0.15, 0.1, 0.1, 0.05]
RELEVANT_NUMBER_OF_LAST_GAMES = 3

class EloPredictor(object):


    def __init__(self, persister):

        self.__persister = persister


    def predict(self, just_predict, odds, r1=None, r2=None):

        if just_predict:
            self.__hteam = odds['home_team']['name']['fdata_name']
            self.__ateam = odds['away_team']['name']['fdata_name']

        else:
            self.__hteam = odds['home_team.name.fdata_name']
            self.__ateam = odds['away_team.name.fdata_name']

        ht_h_fixs = self.__get_last_games("home_team.name.fdata_name",self.__hteam, odds['match_date'])
        ht_a_fixs = self.__get_last_games("away_team.name.fdata_name",self.__hteam, odds['match_date'])
        at_a_fixs = self.__get_last_games("away_team.name.fdata_name",self.__ateam, odds['match_date'])
        at_h_fixs = self.__get_last_games("home_team.name.fdata_name",self.__ateam, odds['match_date'])
        if (len(ht_h_fixs) != len(ht_a_fixs)) or (len(ht_h_fixs) != len(at_a_fixs)) or (len(at_a_fixs) != len(at_h_fixs)):
            raise Exception('Not enough games to make prediction')

        prediction, t1, t2 = self.__analyze_games(ht_h_fixs,ht_a_fixs,
                                          at_a_fixs,at_h_fixs,
                                          r1, r2)

        odds.update({"predicted_odds.home_win":prediction[0],
                     "predicted_odds.away_win":prediction[2],
                     "predicted_odds.draw":prediction[1],
                     "home_team.rating": t1,
                     "away_team.rating": t2,
                     "result_chances":prediction[3]})


    def __analyze_games(self, ht_h_fixs, ht_a_fixs, at_a_fixs,
                        at_h_fixs, elo_t1, elo_t2):

        if elo_t1 is None:
            elo_t1 = self.__persister.db['ratings'].find_one({"name.fdata_name":self.__hteam})['rating']
            elo_t2 = self.__persister.db['ratings'].find_one({"name.fdata_name":self.__ateam})['rating']


        h_goals, h_goals_against = self.__analyze_performance(ht_h_fixs, ht_a_fixs, True)
        a_goals, a_goals_against = self.__analyze_performance(at_a_fixs, at_h_fixs, False)
        h_expected_goals = self.__factor_in_opposition(h_goals, a_goals_against,
                                                       elo_t2 - elo_t1)
        a_expected_goals = self.__factor_in_opposition(a_goals, h_goals_against,
                                                       elo_t1 - elo_t2)
        prediction = self.__get_prediction(h_expected_goals, a_expected_goals)

        return prediction, elo_t1, elo_t2


    def  __get_prediction(self, h_prob, a_prob):

        h_goal_probability = []
        a_goal_probability = []
        for i in range(0,6):
            h_goal_probability.append(scipy.stats.poisson.pmf(i, h_prob))
            a_goal_probability.append(scipy.stats.poisson.pmf(i, a_prob))
        results = []
        home_chance = 0
        draw_chance = 0
        away_chance = 0
        for i in range(0,6):
            for p in range(0,6):
                result = "%d : %d"%(i, p)
                chance = h_goal_probability[i] * a_goal_probability[p]
                if i == p: draw_chance += chance
                elif i < p: away_chance += chance
                elif i > p: home_chance += chance
                results.append({"result": result, "chance": chance})
        home_chance = 1/home_chance
        draw_chance = 1/draw_chance
        away_chance = 1/away_chance

        return [math.ceil(home_chance*100)/100, math.ceil(draw_chance*100)/100,
                math.ceil(away_chance*100)/100, results]


    def __factor_in_opposition(self, goals_for, opposition_goals_against,
                               diff_rank):

        opposition_class = 1.0
        if diff_rank > 0:
            opposition_class = opposition_class - diff_rank/200
        elif diff_rank < 0:
            opposition_class = opposition_class + -(diff_rank)/200


        expectation = -(opposition_goals_against * opposition_class - goals_for)
        if expectation > 0:
            return goals_for - expectation
        return goals_for + -(expectation)


    def __analyze_performance(self, more_relevant_fixs, less_relevant_fixs, home_team):

        h_expected_goals_for_average = []
        h_expected_goals_against_average = []
        for f in more_relevant_fixs:
            expected_for, expected_against = self.__get_expected_goals(f, home_team)
            h_expected_goals_for_average.append(expected_for)
            h_expected_goals_against_average.append(expected_against)

        for f in less_relevant_fixs:
            expected_for, expected_against = self.__get_expected_goals(f, not home_team)
            h_expected_goals_for_average.append(expected_for)
            h_expected_goals_against_average.append(expected_against)

        expected_goals_to_conciede = 0
        expected_goals_to_score = 0

        for i in range(0, len(h_expected_goals_for_average)):
            expected_goals_to_conciede += h_expected_goals_against_average[i] * TEAM_GAMES_RELEVANCE[i]
            expected_goals_to_score += h_expected_goals_for_average[i] *  TEAM_GAMES_RELEVANCE[i]
        return expected_goals_to_score, expected_goals_to_conciede


    def __get_expected_goals(self, fixture, home_team):

        team = 'home_team' if home_team else 'away_team'
        opposition = 'away_team' if home_team else 'home_team'

        goals_for = fixture[team]['goals']['full_time']
        goals_against = fixture[opposition]['goals']['full_time']
        shots_for = fixture[team]['shots']['total']
        shots_against = fixture[opposition]['shots']['total']
        target_for = fixture[team]['shots']['on_target']
        target_against = fixture[opposition]['shots']['on_target']

        ratio_for = (goals_for + (shots_for / ELO_GOALS_FROM_TOTAL_SHOTS)  +
                     (target_for / ELO_GOALS_FROM_TARGET_SHOTS)) / 3

        ratio_against = (goals_against + (shots_against / ELO_GOALS_FROM_TOTAL_SHOTS)  +
                     (target_against / ELO_GOALS_FROM_TARGET_SHOTS)) / 3
        return ratio_for, ratio_against


    def __get_last_games(self, team, team_name, date):

        fs = self.__persister.db['fixtures'].find({team: team_name,
                                                   "match_date": {"$lt": date}}).sort('match_date', -1).limit(10)

        fs = list(fs)
        if len(fs) < RELEVANT_NUMBER_OF_LAST_GAMES:
            return fs
        return fs[0:RELEVANT_NUMBER_OF_LAST_GAMES]